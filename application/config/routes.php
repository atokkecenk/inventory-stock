<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Auth/index';

$route['login'] = 'Auth/index';
$route['cek-login'] = 'Auth/login';
$route['logout'] = 'Auth/logout';

$route['dashboard'] = 'Home/dashboard';

// Stock IN
$route['stock-in'] = 'Home/stockIn';
$route['save-stock-in'] = 'Home/saveStockIn';

// Stock OUT
$route['stock-out'] = 'Home/stockOut';

// Resources
$route['src-condition'] = 'Home/getCondition';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
