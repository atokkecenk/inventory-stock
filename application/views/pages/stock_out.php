<style>
    #seqReader {
        padding: 20px !important;
        border: 1.5px solid #b2b2b2 !important;
        border-radius: 8px;
    }

    #seqReader img[alt="Info icon"] {
        display: none;
    }

    #seqReader img[alt="Camera based scan"] {
        width: 100px !important;
        height: 100px !important;
    }

    #html5-qrcode-anchor-scan-type-change {
        text-decoration: none !important;
        color: #1d9bf0;
    }

    #qr-canvas-visible {
        width: 222px !important;
        height: 200px !important;
        display: inline-block;
        margin-top: 2rem;
    }
</style>
<div class="card mb-4">
    <div class="card-header"><strong>Form Stock Out</strong></div>
    <div class="card-body">
        <?php
        if ($this->session->flashdata('success')) {
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    ' . $this->session->flashdata('success') . '
                    <button type="button" class="btn-close" data-coreui-dismiss="alert" aria-label="Close"></button>
                </div>';
        } elseif ($this->session->flashdata('error')) {
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ' . $this->session->flashdata('error') . '
                    <button type="button" class="btn-close" data-coreui-dismiss="alert" aria-label="Close"></button>
                </div>';
        }
        ?>
        <form action="#" method="post">
            <div class="mb-3 col-sm-3">
                <label class="form-label">CN Number</label>
                <input type="text" class="form-control" name="cn_number">
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Sequence Number</th>
                            <th class="text-center">Rep Number</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">CRM Number</th>
                            <th class="text-center">Serial Number</th>
                            <th class="text-center">Send to FSL Location</th>
                            <th class="text-center">Part Condition</th>
                            <th class="text-center">Courier Service</th>
                            <th class="text-center">Remark</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody"></tbody>
                </table>
            </div>
            <button class="btn btn-md btn-primary" id="addBtn" type="button">
                Add New Row
            </button>
        </form>
    </div>
</div>

<script>
    function documentReady() {

    }
</script>