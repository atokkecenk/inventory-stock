<style>
    #seqReader {
        padding: 20px !important;
        border: 1.5px solid #b2b2b2 !important;
        border-radius: 8px;
    }

    #seqReader img[alt="Info icon"] {
        display: none;
    }

    #seqReader img[alt="Camera based scan"] {
        width: 100px !important;
        height: 100px !important;
    }

    #html5-qrcode-anchor-scan-type-change {
        text-decoration: none !important;
        color: #1d9bf0;
    }

    #qr-canvas-visible {
        width: 222px !important;
        height: 200px !important;
        display: inline-block;
        margin-top: 2rem;
    }
</style>
<div class="card mb-4">
    <div class="card-header"><strong>Form Stock In</strong></div>
    <div class="card-body">
        <?php
        if ($this->session->flashdata('success')) {
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    ' . $this->session->flashdata('success') . '
                    <button type="button" class="btn-close" data-coreui-dismiss="alert" aria-label="Close"></button>
                </div>';
        } elseif ($this->session->flashdata('error')) {
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ' . $this->session->flashdata('error') . '
                    <button type="button" class="btn-close" data-coreui-dismiss="alert" aria-label="Close"></button>
                </div>';
        }
        ?>
        <form action="save-stock-in" method="post">
            <div class="mb-3">
                <button class="btn btn-primary" id="cnReader" type="button"></button>
                <button class="btn btn-primary" id="seqReader" type="button"></button>
                <button class="btn btn-primary" id="repReader" type="button"></button>
            </div>
            <div class="mb-3">
                <label class="form-label">CN Number</label>
                <div class="msgCN"></div>
                <div class="input-group">
                    <input class="form-control" type="text" name="cn_number" id="cnText">
                    <button class="btn btn-primary" id="cnButton" type="button">Scan</button>
                </div>
            </div>
            <div class="mb-3">
                <label class="form-label">Sequence No</label>
                <div class="msgSeq"></div>
                <div class="input-group">
                    <input class="form-control" type="text" name="sequence_number" id="seqText">
                    <button class="btn btn-primary" id="seqButton" type="button">Scan</button>
                </div>
            </div>
            <div class="mb-3">
                <label class="form-label">REP</label>
                <div class="msgRep"></div>
                <div class="input-group">
                    <input class="form-control" type="text" name="rep" id="repText">
                    <button class="btn btn-primary" id="repButton" type="button">Scan</button>
                </div>
            </div>
            <div class="mb-3">
                <label class="form-label">Quantity</label>
                <input type="number" min="0" class="form-control" name="qty">
            </div>
            <div class="mb-3">
                <label class="form-label">CRM Number</label>
                <input type="text" class="form-control" name="crm_number">
            </div>
            <div class="mb-3">
                <label class="form-label">Serial Number</label>
                <input type="text" class="form-control" name="serial_number">
            </div>
            <div class="mb-3">
                <label class="form-label">From FSL Location</label>
                <input type="text" class="form-control" name="fsl_location">
            </div>
            <div class="mb-3">
                <label class="form-label">Part Condition</label>
                <select class="form-select" name="condition">
                    <option value="">- Please Select One -</option>
                    <?php
                    foreach ($condition as $cd) {
                        echo "<option value='$cd->id'>$cd->condition</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="mb-3">
                <label class="form-label">Courier Service</label>
                <input type="text" class="form-control" name="courier_service">
            </div>
            <div class="mb-3">
                <label class="form-label">Remark</label>
                <textarea class="form-control" rows="2" name="remark"></textarea>
            </div>
            <div class="d-grid gap-2">
                <button class="btn btn-primary" type="submit" name="save"><i class="icon cil-send"></i> Save</button>
            </div>
        </form>
    </div>
</div>

<script>
    function documentReady() {

    }
</script>