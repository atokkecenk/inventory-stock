</div>
<footer class="footer">
    <div><a href="https://coreui.io">CoreUI </a><a href="#">Bootstrap Admin Template</a> © 2023 creativeLabs.</div>
</footer>
</div>
<!-- CoreUI and necessary plugins-->
<script src="<?= base_url() ?>assets/themes/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/themes/vendors/simplebar/js/simplebar.min.js"></script>
<!-- Plugins and scripts required by this view-->
<!-- <script src="<?= base_url() ?>assets/themes/vendors/chart.js/js/chart.min.js"></script> -->
<script src="<?= base_url() ?>assets/themes/vendors/@coreui/chartjs/js/coreui-chartjs.js"></script>
<script src="<?= base_url() ?>assets/themes/vendors/@coreui/utils/js/coreui-utils.js"></script>
<!-- <script src="<?= base_url() ?>assets/themes/js/main.js"></script> -->

<!-- Jquery -->
<script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
<script>
    function documentReady() {
        if (typeof this.documentReady === "function") {
            documentReady();
        } else {
            console.log("documentReady not available!");
        }
    }

    $(document).ready(function() {
        // documentReady();

        $('#cnReader, #seqReader, #repReader').css({
            'display': 'none'
        });

        $('#cnButton').click(function() {
            $('#cnReader').css({
                'display': 'block',
                'width': '100%'
            });

            $('#seqReader, #repReader').css({
                'display': 'none'
            });

            function domReady(fn) {
                if (
                    document.readyState === "complete" ||
                    document.readyState === "interactive"
                ) {
                    setTimeout(fn, 1000);
                } else {
                    document.addEventListener("DOMContentLoaded", fn);
                }
            }

            domReady(function() {
                function onScanSuccess(decodeText, decodeResult) {
                    $('.msgCN').html(`<div class="alert alert-success p-1" style="font-size: 12px;">Insert CN Number successfully</div>`);
                    $('#cnText').val(decodeText, decodeResult);
                    alert(`Insert CN Number successfully`);
                }

                let htmlscanner = new Html5QrcodeScanner("cnReader", {
                    fps: 10,
                    qrbos: 250,
                });
                htmlscanner.render(onScanSuccess);
            });
        });

        $('#seqButton').click(function() {
            $('#seqReader').css({
                'display': 'block',
                'width': '100%'
            });

            $('#cnReader, #repReader').css({
                'display': 'none'
            });

            function domReady(fn) {
                if (
                    document.readyState === "complete" ||
                    document.readyState === "interactive"
                ) {
                    setTimeout(fn, 1000);
                } else {
                    document.addEventListener("DOMContentLoaded", fn);
                }
            }

            domReady(function() {
                function onScanSuccess(decodeText, decodeResult) {
                    $('.msgSeq').html(`<div class="alert alert-success p-1" style="font-size: 12px;">Insert Sequence Number successfully</div>`);
                    $('#seqText').val(decodeText, decodeResult);
                    alert(`Insert Sequence Number successfully`);
                }

                let htmlscanner = new Html5QrcodeScanner("seqReader", {
                    fps: 10,
                    qrbos: 250,
                });
                htmlscanner.render(onScanSuccess);
            });
        });

        $('#repButton').click(function() {
            $('#repReader').css({
                'display': 'block',
                'width': '100%'
            });

            $('#cnReader, #seqReader').css({
                'display': 'none'
            });

            function domReady(fn) {
                if (
                    document.readyState === "complete" ||
                    document.readyState === "interactive"
                ) {
                    setTimeout(fn, 1000);
                } else {
                    document.addEventListener("DOMContentLoaded", fn);
                }
            }

            domReady(function() {
                function onScanSuccess(decodeText, decodeResult) {
                    $('.msgRep').html(`<div class="alert alert-success p-1" role="alert" style="font-size: 12px;">Insert REP Number successfully</div>`);
                    $('#repText').val(decodeText, decodeResult);
                    alert(`Insert REP Number successfully`);
                }

                let htmlscanner = new Html5QrcodeScanner("repReader", {
                    fps: 10,
                    qrbos: 200,
                });
                htmlscanner.render(onScanSuccess);
            });
        });

        $('#seqTextOut').keyup(function() {
            var n = $(this).val();
            $.ajax({
                type: 'GET',
                url: '',
                beforeSend: function() {
                    console.log();
                },
                success: function(res) {
                    console.log(res);
                }
            });
        });

        let count = 1;
        $('#addBtn').click(function() {
            $('#sidebar').addClass('sidebar-narrow-unfoldable');

            let dynamicRowHTML = `<tr class="rowClass""> 
                                <td class="row-index text-center" width="170">
                                    <input type="text" class="form-control seqNumb seq-numb${count}" name="seq_number[]">
                                </td> 
                                <td class="row-index text-center" width="170">
                                    <input type="text" class="form-control repNumb rep-numb${count}" name="rep_number[]">
                                </td> 
                                <td class="row-index text-center" width="100">
                                    <input type="number" class="form-control" name="qty[]">
                                </td> 
                                <td class="row-index text-center" width="170">
                                    <input type="text" class="form-control" name="crm_number[]">
                                </td> 
                                <td class="row-index text-center" width="170">
                                    <input type="text" class="form-control" name="serial_number[]">
                                </td>
                                <td class="row-index text-center" width="170">
                                    <input type="text" class="form-control" name="fsl_location[]">
                                </td>
                                <td class="row-index text-center" width="170">
                                    <select class="form-select select-conditiion select${count}" name="condition[]">
                                        <option value=''>- Select One -</option>
                                    </select>
                                </td> 
                                <td class="row-index text-center" width="170">
                                    <input type="text" class="form-control" name="courier_service[]">
                                </td>
                                <td class="row-index text-center" width="170">
                                    <input type="text" class="form-control" name="remark[]">
                                </td> 
                                <td class="text-center"> 
                                    <button class="btn btn-danger btn-sm remove"
                                        type="button"><i class="icon cil-trash text-white"></i>
                                    </button> 
                                </td> 
                            </tr>`;
            $('#tbody').append(dynamicRowHTML);
            count++;
        });

        $('#tbody').on('click', '.remove', function() {
            $(this).parent('td.text-center').parent('tr.rowClass').remove();
        });

        $(document).on('click', '.select-conditiion', function() {
            let id = $(this)[0]['attributes'][0].value;
            id = id.split(' ');
            if ($(this).val() == '') {
                $.ajax({
                    type: 'get',
                    url: 'src-condition',
                    success: function(res) {
                        $(`.${id[2]}`).html(res);
                    }
                });
            }
        });

        $(document).on('keyup', '.seqNumb', function() {
            // let id = $(this);
            // let value = $(this).val();
            // console.log(id);
            // alert('Trial !!')
        });
    });
</script>

</body>

</html>