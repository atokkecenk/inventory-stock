<?php
class SystemModel extends CI_Model
{
    public function getCondition()
    {
        return $this->db->get_where('tb_conditions', ['active' => 1]);
    }
}
