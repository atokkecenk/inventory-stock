<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function login()
	{
		$username   = $this->input->post('username');
		$password   = md5($this->input->post('password'));

		$user = $this->db->get_where('tb_users', ['username' => $username, 'active' => 1])->row();
		if (empty($user)) {
			$this->session->set_flashdata('error', 'Username not found !!');
			redirect('login');
		} else {
			if ($password == $user->password) {
				$session = array(
					'authenticated'	=> 1,
					'id_user'      	=> $user->id,
					'username'      => $user->username,
					'name'      	=> $user->name,
					'level'       	=> $user->level
				);

				$this->session->set_userdata($session);
				redirect('dashboard');
			} else {
				$this->session->set_flashdata('error', '<strong>Oops,</strong> Your password is wrong !!');
				redirect('login');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}
}
