<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('SystemModel', 'sMod');
		if ($this->session->authenticated != 1) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->_pages('dashboard');
	}

	public function dashboard()
	{
		$this->_pages('dashboard');
	}

	public function stockIn()
	{
		$data['condition'] = $this->sMod->getCondition()->result();
		$this->_pages('pages/stock_in', $data);
	}

	function saveStockIn()
	{
		$ins = $this->db->insert('tb_stock', [
			'cn_number' => $_POST['cn_number'],
			'sequence_number' => $_POST['sequence_number'],
			'rep_number' => $_POST['sequence_number'],
			'qty' => $_POST['qty'],
			'crm_number' => $_POST['crm_number'],
			'serial_number' => $_POST['serial_number'],
			'in' => 1,
			'out' => 0,
			'fsl_location' => $_POST['fsl_location'],
			'condition' => $_POST['condition'],
			'courier_service' => $_POST['courier_service'],
			'remark' => $_POST['remark'],
			'date_create' => date('Y-m-d H:i:s'),
			'user_create' => $this->session->id_user,
		]);

		if ($ins) {
			$this->session->set_flashdata('success', 'Success to save Stock In');
			redirect('stock-in');
		}
	}

	public function stockOut()
	{
		$data['condition'] = $this->sMod->getCondition()->result();
		$this->_pages('pages/stock_out', $data);
	}

	public function cekStockIn()
	{
		$get = $this->db->select('*')
			->from('tb_stock')
			->where('in', 1)
			->where('sequence_number', $_GET['seqNum'])
			->or_where('rep_number', $_GET['repNum'])
			->get();
	}

	function getCondition()
	{
		$get = $this->sMod->getCondition()->result();
		echo "<option value=''>- Select One -</option>";
		foreach ($get as $g) {
			echo "<option value='$g->id'>$g->condition</select>";
		}
	}
}
